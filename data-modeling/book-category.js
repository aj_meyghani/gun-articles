module.exports = function(deps) {
  const {uuid, log} = deps;

  function bookCategory(opt) {
    const {db, category, book, categoryName} = opt;
    const linkId = uuid();

    const categoryLink = db.get(linkId).put({
      uuid: linkId,
      type: "Link",
      name: "book_category",
      category_name: categoryName,
    });

    categoryLink.get("book").put(book);
    categoryLink.get("belongs_to").put(category);
    db.get(`category/${categoryName}`).set(categoryLink);

    book.get("categories").set(categoryLink);
    category.get("books").set(categoryLink);

    return categoryLink;
  }

  function createbookCategoryNodes(opt) {
    const {db, categoryNodes, bookNodes} = opt;

    /* Category 1 has two books */
    bookCategory({
      db,
      category: categoryNodes[0],
      book: bookNodes[0],
      categoryName: "Category 1",
    });

    bookCategory({
      db,
      category: categoryNodes[0],
      book: bookNodes[1],
      categoryName: "Category 1",
    });

    /* A book belongs to two categories */
    bookCategory({
      db,
      category: categoryNodes[1],
      book: bookNodes[2],
      categoryName: "Category 2",
    });

    bookCategory({
      db,
      category: categoryNodes[2],
      book: bookNodes[2],
      categoryName: "Category 3",
    });

  }

  function bookCategoryQueries(opt) {
    const {db, categoryNodes, bookNodes} = opt;
    const queries = {

    1: () => {
      /* Log the titles of all the books in Category 1 */
      categoryNodes[0].get("books").map().get("book").get("title").once(log);
    },

    2: () => {
      /* Given a book, log the names of the categories in belongs to */
      bookNodes[0].get("categories").map().get("belongs_to").get("name").once(log);
    },

    3: () => {
      /* Given the categories, log the titles of the book in Category 1 */
      db.get("category/Category 1").map().get("book").get("title").once(log);
    },

    };

    return queries;
  }

  function bookCateogryQueriesPromised(opt) {
    const {db, categoryNodes, bookNodes} = opt;

    const filterMetadata = (o) => {
      const copy = {...o};
      delete copy._;
      return copy;
    };

    const queries = {
      1: () => {
      },

    };

    return queries;
  }

  return {
    createNodes: createbookCategoryNodes,
    queries: bookCategoryQueries,
    queriesPromised: bookCateogryQueriesPromised,
  }
};


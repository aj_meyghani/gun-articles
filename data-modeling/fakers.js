module.exports = function(deps) {
  const {uuid} = deps;

  function fakeRoles() {
    const roles = ["reader", "author", "publisher"];

    return roles.map(role => {
      return {
        uuid: uuid(),
        type: "Role",
        name: role,
      };
    });
  }

  function fakeBooks(opts = {n: 5}) {

    const books = [];
    let howMany = opts.n;

    while(howMany-- > 0) {
      let id = uuid();
      let count = (opts.n - howMany);
      const book = {
        uuid: id,
        type: "Book",
        title: `The Book Title ${count}`,
        subtitle: `Lorem ipsum dolor ${count}`,
        isbn: count,
      };
      books.push(book);
    }

    return books;
  }

  function fakeUsers(opts = {n: 15}) {

    const users = [];
    let howMany = opts.n;

    while(howMany-- > 0) {
      let id = uuid();
      let count = (opts.n - howMany);
      const user = {
        uuid: id,
        type: "User",
        name: `User ${count}`,
        username: `fancy_username${count}`,
        email: `user_${count}@example.com`,
      };

      users.push(user);
    }

    return users;
  }

  function fakeReaders(opts = {n: 5}) {

    const readers = [];
    let howMany = opts.n;

    while(howMany-- > 0) {
      let id = uuid();
      let count = (opts.n - howMany);
      const reader = {
        uuid: id,
        type: "Reader",
        name: `Reader ${count}`,
      };

      readers.push(reader);
    }

    return readers;
  }

  function fakeAuthors(opts = {n: 5}) {

    const authors = [];
    let howMany = opts.n;

    while(howMany-- > 0) {
      let id = uuid();
      let count = (opts.n - howMany);
      const author = {
        uuid: id,
        type: "Author",
        name: `Author ${count}`,
      };

      authors.push(author);
    }

    return authors;
  }

  function fakeCategories(opts = {n: 5}) {

    const categories = [];
    let howMany = opts.n;

    while(howMany-- > 0) {
      let id = uuid();
      let count = (opts.n - howMany);
      const category = {
        uuid: id,
        type: "BookCategory",
        name: `Category ${count}`,
      };

      categories.push(category);
    }

    return categories;
  }

  function fakePublishers(opts = {n: 5}) {

    const publishers = [];
    let howMany = opts.n;

    while(howMany-- > 0) {
      let id = uuid();
      let count = (opts.n - howMany);
      const publisher = {
        uuid: id,
        type: "Publisher",
        name: `Publisher ${count}`,
        address: `${count} Publisher Road, USA`,
      };

      publishers.push(publisher);
    }

    return publishers;
  }

  return {
    fakeBooks,
    fakeReaders,
    fakeAuthors,
    fakeCategories,
    fakePublishers,
    fakeUsers,
    fakeRoles,
  }

};

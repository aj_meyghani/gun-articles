module.exports = function(deps) {
  const {uuid, log} = deps;

  function follow(opt) {
    const {db, sourceNode, destinationNode} = opt;
    const linkId = uuid();

    const followLink = db.get(linkId).put({
      uuid: linkId,
      type: "Link",
      name: "follow",
      date: new Date().toISOString(),
    });
    followLink.get("who").put(destinationNode);
    followLink.get("by").put(sourceNode);

    sourceNode.get("following").set(followLink);
    destinationNode.get("followers").set(followLink);

    return followLink;
  }

  function createFollowNodes(opt) {
    const {db, readerNodes, authorNodes} = opt;

    /* Reader 1 follows Reader 2 */
    follow({
      db,
      sourceNode: readerNodes[0],
      destinationNode: readerNodes[1],
    });

    /* Reader 3 follows Reader 2 */
    follow({
      db,
      sourceNode: readerNodes[2],
      destinationNode: readerNodes[1],
    });

    /* Reader 2 follows Reader 4 */
    follow({
      db,
      sourceNode: readerNodes[1],
      destinationNode: readerNodes[3],
    });

    /* Reader 2 follows Author 1 */
    follow({
      db,
      sourceNode: readerNodes[1],
      destinationNode: authorNodes[0],
    });

    /* Author 1 follows Reader 2 also */
    follow({
      db,
      sourceNode: authorNodes[0],
      destinationNode: readerNodes[1],
    });
  }

  function followQueries(opt) {
    const {db, readerNodes, authorNodes} = opt;
    const queries = {

    1: () => {
      /* Log the name of all the people that Reader 2 follows */
      readerNodes[1].get("following").map().get("who").get("name").once(log);
    },

    2: () => {
      /* Log the name of all the people that are following Reader 2 */
      readerNodes[1].get("followers").map().get("by").get("name").once(log);
    },

    3: () => {
      /* given two people, find if they have any followers in common, if so what
      are their names */
      // TODO.
    },

    };

    return queries;
  }

  function followQueriesPromised(opt) {
    const {db, readerNodes, authorNodes} = opt;

    const filterMetadata = (o) => {
      const copy = {...o};
      delete copy._;
      return copy;
    };

    const queries = {
      1: () => {

      },
    };

    return queries;
  }

  return {
    createNodes: createFollowNodes,
    queries: followQueries,
    queriesPromised: followQueriesPromised,
  }
};


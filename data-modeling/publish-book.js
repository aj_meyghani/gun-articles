module.exports = function(deps) {
  const {uuid, log} = deps;

  function publishBook(opt) {
    const {db, publisher, book, date} = opt;
    const linkId = uuid();

    const publishLink = db.get(linkId).put({
      uuid: linkId,
      type: "Link",
      name: "publish_book",
      date: date,
    });
    publishLink.get("book").put(book);
    publishLink.get("publisher").put(publisher);
    book.get("publication_details").put(publishLink);
    publisher.get("books").set(publishLink);

    return publishLink;
  }

  function createPublishBookNodes(opt) {
    const {db, publisherNodes, bookNodes} = opt;

    /* Publisher 1 publishes one book */
    publishBook({
      db,
      publisher: publisherNodes[0],
      book: bookNodes[0],
      date: new Date().toISOString(),
    });

    /* Publisher 2 publishes two books */
    publishBook({
      db,
      publisher: publisherNodes[1],
      book: bookNodes[1],
      date: new Date().toISOString(),
    });
    publishBook({
      db,
      publisher: publisherNodes[1],
      book: bookNodes[2],
      date: new Date().toISOString(),
    });
  }

  function publishBookQueries(opt) {
    const {db, publisherNodes, bookNodes} = opt;
    const queries = {

    1: () => {
      /* Log the titles of the books publisher by publisher 1 */
      publisherNodes[0].get("books").map().get("book").get("title").once(log);
    },

    2: () => {
      /* Log the titles of the books publisher by publisher 2 */
      publisherNodes[1].get("books").map().get("book").get("title").once(log);
    },

    3: () => {
      /* Given book 1, log the name of its publisher */
      bookNodes[0].get("publication_details").get("publisher").get("name").once(log);
    },

    4: () => {
      /* Given book 1, log the date it was published */
      bookNodes[0].get("publication_details").get("date").once(log);
    }

    };

    return queries;
  }

  function publishBookQueriesPromised(opt) {
    const {db, publisherNodes, bookNodes} = opt;

    const filterMetadata = (o) => {
      const copy = {...o};
      delete copy._;
      return copy;
    };

    const queries = {
      1: () => {
      },

    };

    return queries;
  }

  return {
    createNodes: createPublishBookNodes,
    queries: publishBookQueries,
    queriesPromised: publishBookQueriesPromised,
  }
};


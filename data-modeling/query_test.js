const log = require("./log");
const db = require("gun")();
require("gun/lib/then");
const q = require("./query");

const publisher1 = db.get("publishers/1").put({name: "Publisher 1"});
const publisher2 = db.get("publishers/2").put({name: "Publisher 2"});
const publishers = db.get("publishers");
publishers.set(publisher1);
publishers.set(publisher2);

const rev1 = db.get("reviews/1").put({name: "review 1"});
const rev2 = db.get("reviews/2").put({name: "review 2"});
const revs = db.get("reviews");
revs.set(rev1);
revs.set(rev2);

const b1 = db.get("book/1").put({title: "b1"});
b1.get("publisher").put(publisher1);
b1.get("reviews").set(rev1);
b1.get("reviews").set(rev2);
const b2 = db.get("book/2").put({title: "b2"});
b2.get("publisher").put(publisher2);
const books = db.get("books");
books.set(b1);
books.set(b2);

const r1 = db.get("reader/1").put({name: "reader 1"});
r1.get("books").put(books);

const r2 = db.get("reader/2").put({name: "reader 2"});

rev1.get("reader").put(r1);
rev2.get("reader").put(r2);

setTimeout(() => {
  const q1 = q(db).get("book/1").data().then(log);
  const q2 = q(db).get("book/1").get("reviews").getSet().data().then(log);
  const q3 = q(db).get("publishers").getSet().get("name").data().then(log);
  const q4 = q(db).get("publishers/1").data().then(log);
  const q5 = q(db).get("book/1").get("publisher").data().then(log);
}, 100 /* adjust this number so that the data is ready */);


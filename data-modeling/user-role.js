module.exports = function(deps) {
  const {uuid, log} = deps;

  function userRole(opt) {
    const {db, name, userTypeNode, user} = opt;
    const linkId = uuid();

    const roleLink = db.get(linkId).put({
      uuid: linkId,
      type: "Link",
      name: "role",
      role_name: name,
    });

    roleLink.get("user").put(user);
    roleLink.get("assigned_to").put(userTypeNode);
    db.get(`roles/${name}`).set(roleLink);
    const userUuid = user._.put.uuid; // HACK, DONT DO THIS IN ACTUAL APP
    const userType = userTypeNode._.put.type.toLowerCase() + "s"; // HACK, DONT DO THIS IN ACTUAL APP
    db.get(`users/${userUuid}`).set(user);
    db.get('users').set(user);
    db.get(userType).set((userTypeNode));

    user.get("roles").set(roleLink);
    userTypeNode.get("role").put(roleLink);

    return roleLink;
  }

  function createRoleLinkNodes(opt) {
    const {db, userNodes, fakeRoles, readerNodes, authorNodes, publisherNodes} = opt;

    /* first 5 users are readers */
    userNodes.slice(0, 5).forEach((u, i) => {
      userRole({
        db,
        name: "reader",
        user: u,
        userTypeNode: readerNodes[i],
      });
    });
    // user 1 is also an author
    userRole({
      db,
      name: "author",
      user: userNodes[0],
      userTypeNode: userNodes[0],
    })

    /* next 5 users are authors */
    userNodes.slice(5, 10).forEach((u, i) => {
      userRole({
        db,
        name: "author",
        user: u,
        userTypeNode: authorNodes[i],
      });
    });

    /* next 5 users are publishers */
    userNodes.slice(10, 15).forEach((u, i) => {
      userRole({
        db,
        name: "publisher",
        user: u,
        userTypeNode: publisherNodes[i],
      });
    });

  }

  function roleQueries(opt) {
    const {db, userNodes} = opt;
    const queries = {

    1: () => {
      /* Log the roles of user 1 */
      userNodes[0].get("roles").map().get("role_name").once(log);
    },

    2:() => {
      /* Log the names of all the readers */
      db.get("readers").map().get("name").once(log);
    },

    3:() => {
      /* Log the names of all the authors */
      db.get("authors").map().get("name").once(log);
    },

    4:() => {
      /* Log the names of all the publishers */
      db.get("publishers").map().get("name").once(log);
    },

    5:() => {
      /* Log the emails of all the users */
      db.get("users").map().get("email").once(log);
    }

    };

    return queries;
  }

  function roleQueriesPromised(opt) {
    const {db, userNodes} = opt;

    const filterMetadata = (o) => {
      const copy = {...o};
      delete copy._;
      return copy;
    };

    const queries = {
      1: () => {
      },

    };

    return queries;
  }

  return {
    createNodes: createRoleLinkNodes,
    queries: roleQueries,
    queriesPromised: roleQueriesPromised,
  }
};


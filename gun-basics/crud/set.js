const n1 = db.get('119581').put({
  uuid: '119581',
  name: 'node 1',
});
const n2 = db.get('119515').put({
  uuid: '119515',
  name: 'node 2',
});
const group = db.get('8871'); // create a group node
group.set(n1);
group.set(n2);
